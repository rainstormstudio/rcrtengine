#include "RCRTEngine.hpp"

struct Object {
  char ch;
  f32 speed;
  f32 posX;
  f32 posY;
  Color fg;
  Color bg;
};

i32 round(f32 value) {
  return static_cast<i32>(value + 0.5f);
}

class Demo : public RCRTEngine {
  Object obj;
public:
  bool start() override {
    obj = {'@', 2.0f, 0, 5, Color::BLACK, Color::BLUE};

    return true;
  }

  bool update(f32 deltaTime) override {
    if (obj.posX + obj.speed * deltaTime < 0 ||
        obj.posX + obj.speed * deltaTime >= windowWidth) {
      obj.speed = -obj.speed;
    }
    obj.posX += obj.speed * deltaTime;
    return true;
  }

  bool render(f32 deltaTime) override {
    fill(5, 5, 10, 10, ' ', Color::BLACK, Color::YELLOW);
    write(0, 1, "FPS:" + std::to_string(1.0f / deltaTime), Color::RED, Color::BLACK);
    draw(round(obj.posX), round(obj.posY), obj.ch, obj.fg, obj.bg);
    fill(0, 0, 4, 1);
    write(0, 0, "test");
    return true;
  }
};

int main() {
  Demo demo{};

  if (demo.init()) {
    demo.run();
  }

  return 0;
}
