#include <chrono>
#include <cstdint>
#include <iostream>
#include <ncurses.h>
#include <ratio>
#include <string>
#include <vector>

// type definitions

using i8 = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

using f32 = float;
using f64 = double;

// colors

enum class Color {
  BLACK = 0,
  BLUE,
  GREEN,
  CYAN,
  RED,
  MAGENTA,
  YELLOW,
  WHITE,

  B_BLACK,
  B_BLUE,
  B_GREEN,
  B_CYAN,
  B_RED,
  B_MAGENTA,
  B_YELLOW,
  B_WHITE,

  NONE
};

// engine

class RCRTEngine {

  // pixel cell
  struct CellTexture {
    char ch;
    Color foreColor;
    Color backColor;
  };

  bool m_loop; // game looping status
  WINDOW* m_window;
  std::vector<std::vector<CellTexture>> m_buffer;

protected:
  i32 windowPosX;
  i32 windowPosY;
  i32 windowWidth;
  i32 windowHeight;

public:
  RCRTEngine() {
    windowPosX = 0;
    windowPosY = 0;
    windowWidth = 0;
    windowHeight = 0;
    m_loop = false;
    m_window = nullptr;
  }
  virtual ~RCRTEngine() {}

  bool init(i32 width = 30, i32 height = 10, i32 posX = 0, i32 posY = 0) {
    windowPosX = posX;
    windowPosY = posY;
    windowWidth = width;
    windowHeight = height;

    initscr();
    if (!has_colors()) {
      std::cerr << "color not supported" << std::endl;
      endwin();
      return false;
    }
    initColors();

    noecho();
    curs_set(false);

    m_window = newwin(windowHeight, windowWidth, windowPosY, windowPosX);

    m_buffer = std::vector<std::vector<CellTexture>>(windowHeight);
    for (size_t i = 0; i < windowHeight; i ++) {
      m_buffer[i] = std::vector<CellTexture>(windowWidth);
      for (size_t j = 0; j < windowWidth; j ++) {
        m_buffer[i][j].ch = ' ';
      }
    }

    return true;
  }

  void run() {
    m_loop = true;
    gameLoop();
  }

private:
  inline i32 isBrightColor(Color color) {
    i32 i = 1 << 3;
    return i & static_cast<i32>(color);
  }

  inline i8 toCursesColor(Color color) {
    switch (7 & static_cast<i32>(color)) {
    case 0: return COLOR_BLACK;
    case 1: return COLOR_BLUE;
    case 2: return COLOR_GREEN;
    case 3: return COLOR_CYAN;
    case 4: return COLOR_RED;
    case 5: return COLOR_MAGENTA;
    case 6: return COLOR_YELLOW;
    case 7: return COLOR_WHITE;
    }
    return COLOR_BLACK;
  }

  inline i32 colorPairIdx(Color fg, Color bg) {
    i32 B = 1 << 7;
    i32 bbb = (7 & static_cast<i32>(bg)) << 4;
    i32 ffff = 7 & static_cast<i32>(fg);
    return (B | bbb | ffff);
  }

  inline void setColor(Color fg, Color bg) {
    attron(COLOR_PAIR(colorPairIdx(fg, bg)));
    if (isBrightColor(fg)) attron(A_BOLD);
  }

  inline void unsetColor(Color fg, Color bg) {
    attroff(COLOR_PAIR(colorPairIdx(fg, bg)));
    if (isBrightColor(fg)) attroff(A_BOLD);
  }

  void initColors() {
    start_color();
    for (i32 bg = 0; bg < 8; bg ++) {
      for (i32 fg = 0; fg < 8; fg ++) {
        init_pair(
                  colorPairIdx(static_cast<Color>(fg), static_cast<Color>(bg)),
                  toCursesColor(static_cast<Color>(fg)),
                  toCursesColor(static_cast<Color>(bg)));
      }
    }
  }

  void gameLoop() {
    if (!start()) m_loop = false;

    auto time_a = std::chrono::high_resolution_clock::now();
    auto time_b = std::chrono::high_resolution_clock::now();

    while (m_loop) {
      while (m_loop) {
        time_b = std::chrono::high_resolution_clock::now();
        f32 deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(time_b - time_a).count() / 1000000.0f;
        time_a = time_b;

        // TODO: input

        if (!update(deltaTime)) m_loop = false;

        clearBuffer();
        if (!render(deltaTime)) m_loop = false;
        renderBuffer();
      }

      if (destroy()) {
        endwin();
      } else {
        m_loop = true;
      }
    }
  }

protected:
  virtual bool start() = 0;
  virtual bool update(f32 deltaTime) = 0;
  virtual bool render(f32 deltaTime) = 0;
  virtual bool destroy() { return true; }

protected:
  inline void draw(i32 x, i32 y, char ch = ' ', Color fg = Color::NONE, Color bg = Color::NONE) {
    if (0 <= x && x < windowWidth && 0 <= y && y < windowHeight) {
      m_buffer[y][x].ch = ch;
      if (fg != Color::NONE) m_buffer[y][x].foreColor = fg;
      if (bg != Color::NONE) m_buffer[y][x].backColor = bg;
    }
  }

  inline void write(i32 x, i32 y, const std::string content, Color fg = Color::NONE, Color bg = Color::NONE) {
    for (i32 i = 0; i < content.length() && x + i < windowWidth; i ++) {
      m_buffer[y][x + i].ch = content[i];
      if (fg != Color::NONE) m_buffer[y][x + i].foreColor = fg;
      if (bg != Color::NONE) m_buffer[y][x + i].backColor = bg;
    }
  }

  inline void fill(i32 x, i32 y, i32 width, i32 height, char ch = ' ', Color fg = Color::BLACK, Color bg = Color::WHITE) {
    for (i32 i = 0; i < height && y + i < windowHeight; i ++) {
      for (i32 j = 0; j < width && x + j < windowWidth; j ++) {
        m_buffer[y + i][x + j].ch = ch;
        if (fg != Color::NONE) m_buffer[y + i][x + j].foreColor = fg;
        if (bg != Color::NONE) m_buffer[y + i][x + j].backColor = bg;
      }
    }
  }

private:
  inline void clearBuffer() {
    erase();
    for (size_t i = 0; i < windowHeight; i ++) {
      for (size_t j = 0; j < windowWidth; j ++) {
        m_buffer[i][j].ch = ' ';
        m_buffer[i][j].foreColor = Color::WHITE;
        m_buffer[i][j].backColor = Color::BLACK;
      }
    }
  }

  inline void renderBuffer() {
    for (size_t i = 0; i < windowHeight; i ++) {
      for (size_t j = 0; j < windowWidth; j ++) {
        setColor(m_buffer[i][j].foreColor, m_buffer[i][j].backColor);
        mvaddch(i, j, m_buffer[i][j].ch);
        unsetColor(m_buffer[i][j].foreColor, m_buffer[i][j].backColor);
      }
    }
    refresh();
  }
};
